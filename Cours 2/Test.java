import java.util.ArrayList;

//package vehicule;
public class Test{
    public static void main(String[] args)
    {
        ArrayList<Vehicule> list = new  ArrayList<Vehicule>();

        list.add(new Voiture("Lamborghini Urus", 4, 2200,4, Propulsion.DIESEL, 478));
        list.add(new Avion("Boeing 747-8", 410, 295000, 13100));
        list.add(new Chasseur("Boeing 747-8", 410, 295000, 13100, 1000));
        list.add(new Planeur("Boeing 747-8", 410, 295000, 13100, 250));
        
        for (Object element : list) 
        { 
            System.out.println(element.toString());
        }
    }
}