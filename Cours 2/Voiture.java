//package vehicule;


class Voiture extends Vehicule implements Motorise 
{
    private int nbPortes;
    private Propulsion propulsion;
    private float puissance;

    public Voiture(String modele, int nombrePlaces, int poids, int nbPortes, Propulsion propulsion, float puissance)
    {
        super(modele, nombrePlaces, poids);
        this.nbPortes = nbPortes;
        this.propulsion = propulsion;
        this.puissance =puissance;
    }

    public int getNbPortes(){return nbPortes;}
    public void setNbPortes(int nbPortes){this.nbPortes = nbPortes;}

    public float getPuissance(){return puissance;}
    public void setPuissance(float puissance){this.puissance = puissance;}

    public float GetConsomation()
    {
        if(this.propulsion ==  Propulsion.ESSENCE) return puissance*5/poids;
        if(this.propulsion ==  Propulsion.DIESEL) return puissance*3/poids;
        return 0;
    }

    @Override
    public String toString()
    {
        return super.toString() + " NbPortes: " + nbPortes  + " Propulsion : " + propulsion + " Puissance: " + puissance + " Consomation: " + this.GetConsomation();
    }
}