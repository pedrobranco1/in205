//package vehicule;

class Chasseur extends Avion implements Motorise
{
    private float puissance;
    public Chasseur(String modele, int nombrePlaces, int poids, int altitudeMax, float puissance)
    {
        super(modele, nombrePlaces, poids, altitudeMax);
        this.puissance =puissance;
    }

    public float getPuissance(){return puissance;}
    public void setPuissance(float puissance){this.puissance = puissance;}
    public float GetConsomation(){return this.puissance / this.poids;}

    @Override
    public String toString()
    {
        return super.toString() + " Consomation: " + this.GetConsomation();
    }



}