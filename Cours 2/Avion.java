//package vehicule;

class Avion extends Vehicule 
{
    private int altitudeMax;

    public Avion(String modele, int nombrePlaces, int poids, int altitudeMax)
    {
        super(modele, nombrePlaces, poids);
        this.altitudeMax = altitudeMax;
    }

    public int getAltitudeMax(){return altitudeMax;}
    public void setAltitudeMax(int altitudeMax){this.altitudeMax = altitudeMax;}

    @Override
    public String toString()
    {
        return super.toString() + " AltMax: " + altitudeMax;
    }



}