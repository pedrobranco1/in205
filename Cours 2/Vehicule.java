
//package vehicule;

abstract class Vehicule 
{
    protected String modele ;
    protected  int poids, nombrePlaces;

    public Vehicule(String modele, int nombrePlaces, int poids){
        this.modele = modele;
        this.nombrePlaces = nombrePlaces;
        this.poids = poids;
    }

    public String getModele(){return modele;}
    public int getNombrePlaces(){return nombrePlaces;}
    public int getPoids(){return poids;}

    public void setModele(String modele){this.modele = modele;}
    public void setNombrePlaces(int nombrePlaces){this.nombrePlaces = nombrePlaces;}
    public void setPoids(int poids){this.poids=poids;}


    @Override
    public String toString()
    {
        return "MODEL: "+modele+" / nombrePlaces: "+nombrePlaces+" /Poids: "+poids;
    }
}