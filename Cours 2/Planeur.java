//package vehicule;

class Planeur extends Avion
{
    private float portance ;
    public Planeur(String modele, int nombrePlaces, int poids, int altitudeMax, float portance )
    {
        super(modele, nombrePlaces, poids, altitudeMax);
        this.portance  =portance ;
    }

    public float getPortance (){return portance ;}
    public void setPortance (float portance ){this.portance  = portance ;}

    @Override
    public String toString()
    {
        return super.toString() + " Portance: " + this.portance ;
    }
}
