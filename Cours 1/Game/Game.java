import java.util.Scanner;
import java.util.Random;

class Game
{
  public static void main(String[] args) 
  {
    Scanner scanner = new Scanner(System.in);
    Random rand = new Random();
    int number = rand.nextInt(100) + 1;
    int lifes = 4;

    System.out.println("************************************************************");
    System.out.println("*                   WELCOME TO THE GAME                    *");
    System.out.println("*        the computer chose a number between 1 and 0       *");
    System.out.println("*              you have five attempts to guess             *");
    System.out.println("*                                                          *");
    System.out.println("*                       Good luck!!!                       *");
    System.out.println("************************************************************");

    System.out.println("Make your attempt:");
    int value = scanner.nextInt();
    
    if(value == number)
    {
        System.out.println("Congratulations you are a winner!!!");
        return;
    }
    else
    {
        if(value < number) System.out.println("Wrong!!! The number is higher \n");
        else System.out.println("Wrong!!! The number is lower \n");
    }

    while(lifes > 0)
    {
        lifes --;
        
        System.out.println("Make your attempt:");
        value = scanner.nextInt();

        if(value == number)
        {
            System.out.println("Congratulations you are a winner!!!");
            return;
        }
        else
        {
            if(value < number) System.out.println("Wrong!!! The number is higher \n");
            else System.out.println("Wrong!!! The number is lower \n");
        }
        
    }
    
    System.out.println("The correct number was " + number);
    System.out.println("You are a loser. Try next time");

  }
}