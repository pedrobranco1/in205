class Tableau 
{
  public static double[] Sort(double[] array, int beg, int end)
  {
    if(beg >= end || end <= beg) return array;

    double [] temp =  array.clone();
    int position = beg;

    for(int i = beg; i <= end - 1; i++)
    {
      if(temp[end] > temp[i])
      {
        array[position] = temp[i];
        position += 1;
      }
    }

    int pivot = position;
    array[position] = temp[end];
    position += 1;

    
    for(int i = beg; i <= end - 1; i++)
    {
      if(temp[end] <= temp[i])
      {
        array[position] = temp[i];
        position += 1;
      }
    }

    array = Sort(array, beg, pivot-1);
    array = Sort(array, pivot+1, end);  

     return array;
  }


  public static void main(String[] args) 
  {
      double[] doubleArray = {1,13,14,12,-1,5,9,-85,25,62,0,1,8,-23,-2,56,-9,12,54,72,22,5};
      doubleArray = Sort(doubleArray, 0 , doubleArray.length-1);

      for(double value: doubleArray)System.out.println(value);
    

  }
}
