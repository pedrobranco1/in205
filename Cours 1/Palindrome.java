class Palindrome {

    public static void print(Object o) 
    {
        System.out.println(o);
    }

    public static boolean estPalindrome(String mot) 
    {
        int i = 0;    
        while(i < mot.length()/2)
        {
            if(mot.charAt(i) != mot.charAt(mot.length()-1-i)) return false;
            i ++;
        }

        return true;
    }

    public static void main(String[] args) 
    {
        boolean pizzaEstUnPalindrome = estPalindrome("pizza");
        print( pizzaEstUnPalindrome ? "pizza est un palindrome" : "pizza n'est pas un palindrome" );
        boolean kayakEstUnPalindrome = estPalindrome("kayak");
        print( kayakEstUnPalindrome ? "kayak est un palindrome" : "kayak n'est pas un palindrome" );
        boolean wasEstUnPalindrome = estPalindrome("wasitacaroracatisaw");
        print(wasEstUnPalindrome ? "'Was it a car or a cat I saw' est un palindrome" : "'Was it a car or a cat I saw' n'est pas un palindrome" );
        boolean cheeseEstUnPalindrome = estPalindrome("cheese");
        print( cheeseEstUnPalindrome ? "cheese est un palindrome" : "cheese n'est pas un palindrome" );
    }
}