package ships;

public class Destroyer extends AbstractShip
{
    //Constructor size = 2
    public Destroyer(Orientation orientation) {super('D', "Destroyer", 2, orientation);}

    //Change orientation
    public void SetOrientation(Orientation orientation){super.SetOrientation(orientation);}
    
    //Get variables values
    public String GetName() {return super.GetName();}
    public char GetLabel() {return super.GetLabel();}
    public int GetSize() {return super.GetSize();}
    public Orientation GetOrientation() {return super.GetOrientation();}
    
}