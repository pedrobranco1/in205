package ships;

/*
Class declared as abstract because it will serve as the basis for the creation 
of all existing types of ships. It must be public because access to it must be 
allowed outside the "child" classe. We will use it as a generic type of ship.
*/
public abstract class AbstractShip 
{

    //Mandatory basic attributes for every ship.They are Protected because they must not be changed directly by processes outside child classe
    protected String name ;
    protected char label;
    protected  int size;
    protected Orientation orientation;

    protected int strikeCount = 0;

    //Constructor: all ships must have a name, size, orientation and label
    public AbstractShip(char label, String name, int size, Orientation orientation)
    {
        this.name = name;
        this.label = label;
        this.size = size;
        this.orientation = orientation;
    }
    
    //Change orientation
    public void SetOrientation(Orientation orientation){this.orientation = orientation;}

    //Function called when the opponent hits a part of the ship
    public void addStrike(){this.strikeCount ++;}

    //Variable indicating whether the ship sank or not
    public boolean isSunk(){if(this.strikeCount >= this.size)return true; return false;}

    //Get variables values
    public String GetName() {return this.name;}
    public char GetLabel() {return this.label;}
    public int GetSize() {return this.size;}
    public Orientation GetOrientation() {return this.orientation;}


}