package ships;

public class Submarine extends AbstractShip
{
    //Constructor size = 3
    public Submarine(Orientation orientation) {super('S', "Submarine", 3, orientation);}

    //Change orientation
    public void SetOrientation(Orientation orientation){super.SetOrientation(orientation);}
    
    //Get variables values
    public String GetName() {return super.GetName();}
    public char GetLabel() {return super.GetLabel();}
    public int GetSize() {return super.GetSize();}
    public Orientation GetOrientation() {return super.GetOrientation();}
    
}