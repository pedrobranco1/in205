package ships;

public class BattleShip extends AbstractShip
{
    //Constructor size = 4
    public BattleShip(Orientation orientation) {super('B', "BattleShip", 4, orientation);}

    //Change orientation
    public void SetOrientation(Orientation orientation){super.SetOrientation(orientation);}
    
    //Get variables values
    public String GetName() {return super.GetName();}
    public char GetLabel() {return super.GetLabel();}
    public int GetSize() {return super.GetSize();}
    public Orientation GetOrientation() {return super.GetOrientation();}
}