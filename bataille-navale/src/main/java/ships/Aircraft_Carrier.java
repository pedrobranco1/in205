package ships;

public class Aircraft_Carrier extends AbstractShip
{
    //Constructor size = 5
    public Aircraft_Carrier(Orientation orientation) {super('C', "Aircraft_Carrier", 5, orientation);}

    //Change orientation
    public void SetOrientation(Orientation orientation){super.SetOrientation(orientation);}
    
    //Get variables values
    public String GetName() {return super.GetName();}
    public char GetLabel() {return super.GetLabel();}
    public int GetSize() {return super.GetSize();}
    public Orientation GetOrientation() {return super.GetOrientation();}
    
}