package ships;


//Class created to keep information about a position (x, y) of a given ship on the map
public class ShipState
{
    protected AbstractShip ship;

    //
    protected boolean struck = false;

    //Constructor: ship to which the class refers
    public ShipState(AbstractShip ship)
    {
        this.ship = ship;
    }

    //If the opponent has hit the position in which the "shipstate" object refers
    public void addStrike()
    {   
        //If that part has not yet been struck, we inform the ship that a part of it has been struck
        if(this.struck == false) ship.addStrike();
        this.struck = true;
    }

    //Informs if the ship part was struck or not
    public boolean isStruck(){return this.struck;}

    //Informs if the ship was sunk or not
    public boolean isSunk(){return ship.isSunk();}

    //Return the reference ship
    public AbstractShip getShip(){return this.ship;} 

    @Override
    public String toString()
    {   
        //If the part was struck we print it in red
        if(this.struck == true) return ColorUtil2.colorize(this.ship.GetLabel(), ColorUtil2.Color.RED);
        return String.valueOf(this.ship.GetLabel());
    }

}