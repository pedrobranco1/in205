package ensta;
import java.io.Serializable;
import java.util.*;
import ships.*;

public class Player {
    
    
    /* **
     * Attributs
     */
    protected Board board;
    protected Board opponentBoard;
    protected int destroyedCount;
    protected AbstractShip[] ships;
    protected int ships_size;
    protected boolean lose;


    //Constructor
    public Player(Board board, Board opponentBoard, List<AbstractShip> ships) 
    {
        this.board = board;
        this.ships_size = ships.size();
        this.ships = ships.toArray(new AbstractShip[0]);
        this.opponentBoard = opponentBoard;
    }

    //Function responsible for placing ships on the board
    public void putShips() 
    {
        int i = 0;
        while (i < this.ships_size)
        {
            //Asking the player for a position and orientation to place the ship i
            String msg = String.format("placer %d : %s(%d)", i + 1, ships[i].GetName(), ships[i].GetSize());
            System.out.println(msg);

            //Taking the position and orientation entered by the player
            InputHelper.ShipInput res = InputHelper.readShipInput();


            //Placing the ship in the orientation entered by the player
            if(Arrays.asList(res.orientation).contains("n"))ships[i].SetOrientation(Orientation.NORTH);
            else if(Arrays.asList(res.orientation).contains("s"))ships[i].SetOrientation(Orientation.SOUTH);
            else if(Arrays.asList(res.orientation).contains("e"))ships[i].SetOrientation(Orientation.EAST);
            else if(Arrays.asList(res.orientation).contains("w"))ships[i].SetOrientation(Orientation.WEST);

            try
            {
                board.putShip(ships[i],res.x,res.y);
            }
            catch(ArrayIndexOutOfBoundsException e)
            {   
                //If the user has chosen an invalid position we must continue on the same ship and request a new position
                System.err.println("Position incorrect! Entrez la position sous forme 'A0 n'");
                i--;
            }

            //Clear Screen
            System.out.print("\033[H\033[2J");  
            System.out.flush();  
            
            board.print();
            ++i;//next ship
        } 
    }

    //Sends a hit to the opponent's board
    public Hit sendHit() 
    {
        boolean done = false;
        Hit hit = null;

        do {

            //Asking the player for a position to shoot
            System.out.println("Where to shoot?");
            InputHelper.CoordInput hitInput = InputHelper.readCoordInput();

            //Checking if the position is inside the board
            if(hitInput.x >= board.getSize()|| hitInput.y >= board.getSize() || hitInput.x < 0 || hitInput.y < 0)
                System.out.println("Invalid Position");

            else
            {   
                //Checking if the position has not already been struck
                if (board.getHit(hitInput.x,hitInput.y) != null) 
                    System.out.println("You have already shot here");

                else 
                {
                    //If the position is possible then we send it to the opponent's board
                    done = true;
                    hit = this.opponentBoard.sendHit(hitInput.x, hitInput.y);


                    if(hit != Hit.MISS) 
                    {   
                        //If the shot was in any ship then we updated the shooting board to true in the struck position
                        this.board.setHit(true, hitInput.x, hitInput.y);

                        //Checking if the struck ship sank
                        if (hit != Hit.STRIKE) System.out.println(hit.toString() + " was sunk!!");
                        else System.out.println(hit.toString());
                    }
                    else 
                    {   
                        //If the shot was in the water then we updated the shooting board to false in the struck position
                        this.board.setHit(false, hitInput.x, hitInput.y);
                        System.out.println(hit.toString());
                    }

                }
        
            }
                
        } while (!done);

        return hit;
    }

    //Return the list of the ships
    public AbstractShip[] getShips() {return ships;}

    //Change the list of ships
    public void setShips(AbstractShip[] ships) {this.ships = ships;}

    //Prints the board with the player's name
    public void print()
    {
        System.out.println(board.getName());
        board.print();
    }
}
