package ensta;
import ships.*;

class Board implements IBoard
{
    /*The variables are private because external objects 
    must not be able to access them directly. They should only 
    be changed by the constructor or internal methods*/
    private int size;
    private String name;
    private ShipState[][] ships;
    private Boolean[][] shots;

    //
    private String[] grid = {"",""};

    //Constructor with specific size passed as parameter
    public Board(String name, int size)
    {
        this.size = size;
        this.name = name;
        this.ships = new ShipState[size][size];
        this.shots = new Boolean[size][size];
        InitBoard();
    }

    //Constructor with standard size = 10
    public Board(String name)
    {
        this.size = 10;
        this.name = name;
        this.ships = new ShipState[size][size];
        this.shots  = new Boolean[size][size];
        InitBoard();
    }

    /*Function responsible for starting the 2 "clean" trays. 
    Set to private to not allow a board to be changed or 
    cleaned out of class in the middle of a game.*/
    private void InitBoard()
    {
        // 65 = 'A'
        char temp = 65;
        for (int i=0; i<size; i++) 
        {   
            //Creating the line with the letters that will serve to guide the game. It also creates a blank board-sized line to help with printing
            this.grid[1] += " " + temp + " ";
            this.grid[0] += "   ";
            temp += 1;
        }
    }

    //Return the board size
    public int getSize(){return size;}

    //Return name
    public String getName(){return name;}

    //Retrun the board of ships
    public ShipState[][] getShip(){return ships;}

    //Return the hit in a given position
    public Boolean getHit(int x, int y){return shots[y][x];}

    //Function responsible for placing a ship in a specific position on the board
    public void putShip(AbstractShip ship, int x, int y) throws ArrayIndexOutOfBoundsException
    {
        //Checking if the position is available and if it is inside the board or if there is a colision
        for(int i = 0; i < ship.GetSize(); i++)
        {
            if(ship.GetOrientation() == Orientation.NORTH)
                if(x < 0 || y - i < 0 || x >= this.size || y -i >= this.size || this.hasShip(y-i,x)) 
                    throw new ArrayIndexOutOfBoundsException("This position has already been taken by another ship or is out of the board");

            if(ship.GetOrientation() == Orientation.SOUTH)
                if(x < 0 || y + i < 0 || x >= this.size || y +i >= this.size || this.hasShip(y+i,x))
                    throw new ArrayIndexOutOfBoundsException("This position has already been taken by another ship or is out of the board");

            if(ship.GetOrientation() == Orientation.WEST)
                if(x-i < 0 || y < 0 || x-i >= this.size || y >= this.size || this.hasShip(y,x-i))
                    throw new ArrayIndexOutOfBoundsException("This position has already been taken by another ship or is out of the board");

            if(ship.GetOrientation() == Orientation.EAST)
                if(x+i< 0 || y < 0 || x+i >= this.size || y >= this.size || this.hasShip(y,x+i))
                    throw new ArrayIndexOutOfBoundsException("This position has already been taken by another ship or is out of the board");
        }

        //Placing the ship
        for(int i = 0; i < ship.GetSize(); i++)
        {
            if(ship.GetOrientation() == Orientation.NORTH)
                ships[y-i][x] = new ShipState(ship);

            if(ship.GetOrientation() == Orientation.SOUTH)
                ships[y+i][x] = new ShipState(ship);

            if(ship.GetOrientation() == Orientation.WEST)
                ships[y][x-i] = new ShipState(ship);

            if(ship.GetOrientation() == Orientation.EAST)
                ships[y][x+i] = new ShipState(ship);
        }
      
    }

    //Checks if the board's position x,y is free.
    public boolean hasShip(int x, int y)
    { 
        if(ships[x][y] != null) return true;
        return false;
    }

    //Change the shots board
    public void setHit(boolean hit, int x, int y){shots[y][x] = hit;}

    
    
    //Sends a hit to the opponent's board
    public Hit sendHit(int x, int y)
    {
        if (ships[y][x] != null)
        {
            this.ships[y][x].addStrike();

            if (ships[y][x].isSunk())
            {
                return Hit.fromInt(ships[y][x].getShip().GetSize());
            }
            if (ships[y][x].isStruck())
            {
                return Hit.STRIKE;
            }
        }

        return Hit.MISS;


    }

    //Printing the ships and shots board
    public void print()
    {   
        System.out.println("Ships:" + grid[0]+ "   "+ "Shots:");
        System.out.println("      " + grid[1]+ "         " + grid[1]);

        for(int i = 0; i < size; i++)
        {
            System.out.printf("%6d", i);  
            for(int j = 0; j < size; j++) 
            {
                if(ships[i][j] != null)
                    System.out.print(" " + ships[i][j].toString() +" ");
                else 
                    System.out.print(" . ");
            }

            System.out.printf("%9d", i);
            for(int j = 0; j < size; j++)
            {
                if(shots[i][j] == null) System.out.print(" . ");
                else
                {
                    if(shots[i][j])  System.out.print(ColorUtil.colorize(" X ", ColorUtil.Color.RED));
                    else System.out.print(" X ");
                }
            }
            System.out.println();
        }    

    }



}