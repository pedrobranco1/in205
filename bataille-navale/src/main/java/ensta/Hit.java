package ensta;
import java.util.NoSuchElementException;
import ships.*;

public enum Hit {
    MISS(-1, "miss"),
    STRIKE(-2, "strike"),
    DESTROYER(2, "Destroyer"),
    SUBMARINE(3, "Submarine"),
    BATTLESHIP(4, "Battleship"),
    CARRIER(5, "Aircraft Carrier");

    /* ***
     * Attributs
     */
    private int value;
    private String label;

    /* ***
     * Constructeur
     */
    Hit(int value, String label) {
        this.value = value;
        this.label = label;
    }

    /* ***
     * Méthodes
     */
    public static Hit fromInt(int value) {
        for (Hit hit : Hit.values()) {
            if (hit.value == value) {
                return hit;
            }
        }
        throw new NoSuchElementException("no enum for value " + value);
    }

    public String toString() {
        return this.label;
    }
};
