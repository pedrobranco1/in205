package ensta;
import java.io.*;
import java.util.*;
import ships.*;

public class Game {

    /* ***
     * Constante
     */
    public static final File SAVE_FILE = new File("savegame.dat");

    /* ***
     * Attributs
     */
    private Player player1;
    private Player player2;
    private Scanner sin;

    /* ***
     * Constructeurs
     */
    public Game() {}

    public Game init() 
    {
        if (!loadSave()) 
        {
            // init attributes
            System.out.println("Enter your name:");

            sin = new Scanner(System.in);

            String name = sin.nextLine();

            //Init boards
            Board b1 = new Board(name,10); 
            Board b2 = new Board("Opponent",10);

            //Init Players
            player1 = new Player(b1, b2, createDefaultShips());
            player2 = new AIPlayer(b2, b1, createDefaultShips());

            // place player ships
            player1.putShips();
            player2.putShips();

        }
        return this;
    }

    /* ***
     * Méthodes
     */
    public void run() 
    {
        int[] coords = new int[2];
        Hit hit;

        // main loop
        
        boolean done;
        do 
        {
            //clear the terminal
            System.out.print("\033[H\033[2J");  
            System.out.flush();   

            //Print the player board
            player1.print();

            //The player makes the shot
            hit = player1.sendHit();
            System.out.printf("\n\n");

            //Check the hit status
            boolean strike = hit != Hit.MISS; 

            //Check end game
            done = updateScore();

            // Save game
            save();

            //If the player has not hit any ships the opponent can play
            if (!done && !strike) 
            {
                do {
                    
                    //The Opponent makes the shot
                    hit = player2.sendHit();
                    System.out.printf("\n\n");

                    //Check the hit status
                    strike = hit != Hit.MISS;
                    
                    //Check end game
                    done = updateScore();

                    //Save Game
                    if (!done) save();
                    
                } while(strike && !done);

            }
            
            //Prints the player's board after the opponent's move
            player1.print();

            //Wait 5 seconds for the player to see the opponent's play
            sleep(5000);
            
             

        } while (!done);

        SAVE_FILE.delete();
        System.out.println(String.format("Player %d won!!", player1.lose ? 2 : 1));
        sin.close();
    }

    private static void sleep(int ms) 
    {
        try {
        Thread.sleep(ms);
        } catch (InterruptedException e) {
        e.printStackTrace();
        }
    }


    private void save() {
       /* try {
            // TODO bonus 2 : uncomment
            //  if (!SAVE_FILE.exists()) {
            //      SAVE_FILE.getAbsoluteFile().getParentFile().mkdirs();
            //  }

            // TODO bonus 2 : serialize players

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    private boolean loadSave() 
    {
        /*if (SAVE_FILE.exists()) {
            try {
                // TODO bonus 2 : deserialize players

                return true;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }*/
        return false;
    }

    private boolean updateScore() {
        for (Player player : new Player[]{player1, player2}) {
            int destroyed = 0;
            for (AbstractShip ship : player.getShips()) {
                if (ship.isSunk()) {
                    destroyed++;
                }
            }

            player.destroyedCount = destroyed;
            player.lose = destroyed == player.getShips().length;
            if (player.lose) {
                return true;
            }
        }
        return false;
    }

    private String makeHitMessage(boolean incoming, int[] coords, Hit hit) {
        String msg;
        ColorUtil.Color color = ColorUtil.Color.RESET;
        switch (hit) {
            case MISS:
                msg = hit.toString();
                break;
            case STRIKE:
                msg = hit.toString();
                color = ColorUtil.Color.RED;
                break;
            default:
                msg = hit.toString() + " coulé";
                color = ColorUtil.Color.RED;
        }
        msg = String.format("%s Frappe en %c%d : %s", incoming ? "<=" : "=>",
                ((char) ('A' + coords[0])),
                (coords[1] + 1), msg);
        return ColorUtil.colorize(msg, color);
    }

    private static List<AbstractShip> createDefaultShips() {
        return Arrays.asList(new AbstractShip[]{new Destroyer(Orientation.NORTH), new Submarine(Orientation.NORTH), new Submarine(Orientation.NORTH), new BattleShip(Orientation.NORTH), new Aircraft_Carrier(Orientation.NORTH)});
    }

    public static void main(String args[]) {
        new Game().init().run();
    }
}
