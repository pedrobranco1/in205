package ensta;

import ships.*;
import java.util.*;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Game game = new Game();

        game.init();

        game.run();

    }
}