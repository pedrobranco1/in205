package ensta;
import java.io.Serializable;
import java.util.*;
import ships.*;

public class AIPlayer extends Player {
    /* **
     * Attribut
     */
    private BattleShipsAI ai;

    /* **
     * Constructeur
     */
    public AIPlayer(Board ownBoard, Board opponentBoard, List<AbstractShip> ships) {
        super(ownBoard, opponentBoard, ships);
        ai = new BattleShipsAI(ownBoard, opponentBoard);
    }

    @Override
    public void putShips (){ai.putShips(ships);}

    
    public Hit sendHit()
    {
        int[] coords = new int[2];
        Hit hit = ai.sendHit(coords);
        return hit;
    }
}
